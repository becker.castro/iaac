resource "aws_launch_template" "lc-grafana" {
  name_prefix   = "lc-grafana"
  image_id      = "ami-0f2b3d4e48207e8b5"
  instance_type = "t2.micro"
}

resource "aws_autoscaling_group" "asg-grafana" {
  availability_zones = [
   "us-east-1a",
   "us-east-1b",
   "us-east-1c",
   "us-east-1d",
   "us-east-1e",
   "us-east-1f"
   ]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  launch_template {
    id      = "${aws_launch_template.lc-grafana.id}"
    version = "$Latest"
  }
}