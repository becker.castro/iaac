resource "aws_launch_template" "lc-prometheus" {
  name_prefix   = "lc-prometheus"
  image_id      = "ami-06a94f24e3e004d21"
  instance_type = "t2.micro"
}

resource "aws_autoscaling_group" "asg-prometheus" {
  availability_zones = [
   "us-east-1a",
   "us-east-1b",
   "us-east-1c",
   "us-east-1d",
   "us-east-1e",
   "us-east-1f"
   ]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  launch_template {
    id      = "${aws_launch_template.lc-prometheus.id}"
    version = "$Latest"
  }
}