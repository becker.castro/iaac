provider "aws" {
    region = "us-east-1"
}
resource "aws_lambda_function" "lambdaffde3a0" {
    filename = "${var.filename}"
    description = "Lambda Function"
    function_name = "${var.lambda-name}"
    handler = "lambda_function.lambda_handler"
    memory_size = 128
    role = "arn:aws:iam::405635533773:role/service-role/teste-caue-lambda-role-5iw5kaye"
    runtime = "${var.python3.6}"
    timeout = 3
    dead_letter_config {
        
    }

    kms_key_arn = ""
    tracing_config {
        mode = "PassThrough"
    }

}
